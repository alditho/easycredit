**Requisitos de instalaci�n**

-Sistema operativo android 5.0(Lollipop) o posterior, que corresponde al 85% de los dispositivos del mercado.

**Instrucciones de uso de aplicaci�n con servidor remoto**

1.Descargar el apk desde :
2.Iniciar sesi�n solo ingresando un nombre de usuario.
3.El siguiente sitio web procesa las solicitudes pendientes : https://www.rabu.mx/easycredit/views/request.php


**Instrucciones de uso con servidor local**

##el proyecto actual con su apk ya funciona con un servidor remoto, estas instrucciones son solo para uso de la aplicaci�n en caso de querer utilizarla de manera local.

## sitio web
1.Instalar xampp con php 7.2.12
2.Clonar en htdocs el proyecto web con el siguiente enlace: git clone https://alditho@bitbucket.org/alditho/easyclient-web.git
3.Descargar base de datos he importarla en phpmyadmin.
4.Modificar archivo global.php con informaci�n de mi servidor local. 
5.Iniciar servicios de xampp.

## aplicacion
1.Instalar android studio.
2.Clona el proyecto android con el siguiente enlace: git clone https://alditho@bitbucket.org/alditho/easycredit.git
3.Ir a el archivo Utils.java he ingresa la direccion ip de servidor local. Ejemplo : http://192.168.0.14/easycredit/api/api.php?op=
4.Correr proyecto en dispositivo movil o generar apk.



**Asunciones**

-El usuario no se debe de repetir en base de datos: 
*para esto genere una consulta sql que buscara usuarios con el username ingresado,
si no encontraba el usuario insertaba en base de datos un nuevo registro, de lo contrario obtenia la informaci�n del usuario.
*el nombre de usuario solo permite numeros y letras sin espacios.

-Dise�o de base de datos:
*Se generaron dos tablas, "user" que continene: USER_ID,username,notificationToken, este token guarda el id del dispositivo del usuario que otorga firebase para enviar notificaciones.
*"request" que contiene: REQUEST_ID,amount,age,creditCard,months,status,FK_USER_ID.  

-Mantener sesi�n iniciada: 
*Si la aplicaci�n movil contiene informaci�n del usuario carga esa informaci�n, de lo contrario lo redirige a iniciar sesi�n.

-Actualizar solicitudes: 
*Esto fue el mayor reto del proyecto, ya que necesite implementar push notifications para avisarle al usuario cuando cambio
el estado de su solicitud y mandar llamar un escuchador para actualizar la informaci�n de las vistas.

-Registrar nueva solicitud: 
*Se dej� el monto m�ximo a solicitar a criterio del usuario, por l�gica propia decid� que la edad m�xima que puede tener un solicitante es de 100 a�os.
*En base de datos se guarda el valor 1 si el usuario tiene tarjeta de cr�dito, de lo contrario se guarda el valor 0.
*Para los meses se guarda en base de datos el n�mero de meses 3,6,9.
*Por defecto la solicitud se registra con status 2 que es pendiente, y los dem�s status son 0: Rechazada. 1: Aceptada.




**Problemas que enfrente al resolver �ste ejercicio**

-Problema con tiempos de ejecuci�n de "cron jobs" ya que primero subi mi proyecto a
000WebHost, pero este solo admite un cron job cada 10 minutos, por eso decid� implementar yo mismo la entra de solicitudes pendientes,
ya que seria muy tedioso para ustedes como evaluadores esperar 10 minutos para que se procesara una solicitud.

-Despues de haber subido a 000WebHost mi proyecto, me di cuenta de que los servidores de ese hosting estaban fallando.

-El tiempo fue en parte un peque�o problema, ya que el proyecto requer�a de la implementaci�n de varias tecnolog�as de terceros, como:

-Push Notifications.
-Sugar orm(Me gustar�a haber utilizado Realm, pero por motivos de agilizar el proyecto utilice sugar orm).
-volley



**Cr�tica constructiva sobre �ste ejercicio**

Considero que es un ejercicio f�cil de desarrollar, que eval�a conocimientos esenciales que un desarrollador debe de tener, pero de igual forma es un ejercicio laborioso ya que tuve que implementar varias tecnolog�as.
Lo considero un buen ejercicio como examen.



