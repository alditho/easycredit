package com.example.aldo.easycredit.Firebase;

import android.util.Log;

import com.example.aldo.easycredit.Utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken(); //obtiene token que otorga firebase
        Log.d("token", "Refreshed token: " + token);
        //se guarda token en sharedpreferences
        Utils.setSharedPreferencesValue(getApplicationContext(),Utils.utilTokenNotification,token); //se guarda en sharedpreferences
    }
}
