package com.example.aldo.easycredit.Requests;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aldo.easycredit.MainActivity;
import com.example.aldo.easycredit.R;
import com.example.aldo.easycredit.Utils.RequestListener;
import com.example.aldo.easycredit.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class NewRequestActivity extends AppCompatActivity{

    private Utils utils= new Utils();
    private EditText editTextQuantity,editTextAge;
    private TextView textViewTotal;
    private RadioGroup radioGroupCredit,radioGroupTime;
    private RadioButton radioButtonThree,radioButtonYes;
    private Button buttonRequest;

    private int creditCardStatus=1;
    private Double amount =0.0;
    private int months=3; //meses seleccionados

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_request);

        setTitle(getResources().getString(R.string.new_request));

        editTextQuantity = findViewById(R.id.editTextQuantity); //se enlaza monto ingresado
        editTextQuantity.addTextChangedListener(new TextWatcher() {//capturar cambios en edittext de cantidad
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {//si cambia el valor de editText

                if(editTextQuantity.getText().toString().trim().length() > 0){//si edittext no esta vacio

                    //calcular la cantidad a pagar con meses seleccionados
                    Double quantity = Double.valueOf(editTextQuantity.getText().toString());
                    if(months==3){ //calcular total a 3 meses
                        amount = quantity +(quantity*0.05);
                        textViewTotal.setText(getResources().getString(R.string.amount_payable)+decimals(amount));
                    }else if(months==6){ //calcular total a 6 meses
                        amount = quantity +(quantity*0.07);
                        textViewTotal.setText(getResources().getString(R.string.amount_payable)+decimals(amount));
                    }else if(months==9){ //calcular total a 12 meses
                        amount = quantity +(quantity*0.12);
                        textViewTotal.setText(getResources().getString(R.string.amount_payable)+decimals(amount));
                    }

                }else{ //si esta vacio, inicializar total a pagar $0.00
                    textViewTotal.setText(getResources().getString(R.string.total));
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        editTextAge = findViewById(R.id.editTextAge);//se enlaza edad del cliente

        //se activa por default 3 meses de plazo
        radioButtonThree = findViewById(R.id.radioButtonThree);
        radioButtonThree.setChecked(true);




        //radiobuttons tarjeta de credito si/no
        radioButtonYes = findViewById(R.id.radioButtonYes);
        radioButtonYes.setChecked(true); //por default tiene tarjeta de credito
        radioGroupCredit = findViewById(R.id.radioGroupCredit);
        //selección del cliente con respecto a su tarjeta de credito
        radioGroupCredit.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.radioButtonYes){
                    creditCardStatus = 1; //tiene tarjeta de credito
                }else if(checkedId == R.id.radioButtonNo){
                    creditCardStatus = 0; //no tiene tarjeta de credito
                }
            }
        });



        //radioGroup de plazo de meses a seleccionar
        radioGroupTime = findViewById(R.id.radioGroupTime);
        radioGroupTime.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(editTextQuantity.getText().toString().equals("")){//si editText candidad esta vacio, se inicializa a $0.00 total a pagar
                    textViewTotal.setText(getResources().getString(R.string.total));
                }else{//si ingreso un monto se calcula el interes
                    Double quantity = Double.valueOf(editTextQuantity.getText().toString());
                    if(checkedId == R.id.radioButtonThree){//3 meses de intereses
                        months=3;
                        amount = quantity +(quantity*0.05);
                        textViewTotal.setText(getResources().getString(R.string.amount_payable)+decimals(amount));
                    }else if(checkedId == R.id.radioButtonSix){//6 meses de intereses
                        months=6;
                        amount = quantity +(quantity*0.07);
                        textViewTotal.setText(getResources().getString(R.string.amount_payable)+decimals(amount));
                    }else if(checkedId == R.id.radioButtonNine){//9 meses de intereses
                        months=9;
                        amount = quantity +(quantity*0.12);
                        textViewTotal.setText(getResources().getString(R.string.amount_payable)+decimals(amount));
                    }
                }//termina else

            }//termina onCheckedChanged
        });


        textViewTotal = findViewById(R.id.textViewtotal); //se enlaza total a pagar en el plazo seleccionado

        //boton para registrar nueva solicitud
        buttonRequest = findViewById(R.id.buttonRequest);
        buttonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextQuantity.getText().toString().equals("")){//si no ingreso monto
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.enter_quantity),Toast.LENGTH_SHORT).show();
                }else if(editTextAge.getText().toString().equals("")){//si no ingreso edad
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.enter_age),Toast.LENGTH_SHORT).show();
                }else if(Integer.parseInt(editTextAge.getText().toString())>100){ //si ingreso que tiene mas de 100 años
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.correct_age),Toast.LENGTH_SHORT).show();
                }else{//de lo contrario realizar nueva solicitud
                    newRequest();
                }

            }
        });


    }//termina onCreate





    //regresar valor con dos decimales
    public String decimals(Double value){

        double myNumber = value;
        DecimalFormat format = new DecimalFormat("0.00");
        System.out.println(format.format(myNumber));

        return format.format(myNumber);
    }


    //Método post para registrar nueva solicitud
    public void newRequest(){

        final JSONObject jsonObject=new JSONObject();
        try {//datos que se envian al servidor
            jsonObject.put("amount",editTextQuantity.getText());
            jsonObject.put("age",editTextAge.getText());
            jsonObject.put("card",creditCardStatus);
            jsonObject.put("months",months);
            jsonObject.put("user_id",Utils.getSharedPreferencesValue(getApplicationContext(),Utils.utilUserId));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        utils.petition(getApplicationContext(), new RequestListener() {
            @Override
            public boolean onRequestFinish(JSONObject response, final ProgressDialog progressDialog) {
                utils.requestComplete(progressDialog);
                if(response!=null){
                    //respuesta del servidor
                    try {

                        //si se registro solicitud
                        if(response.getString("server_response").equals("success")){ //si la respuesta del servidor es correcta

                            //inicia actividad principal de aplicación (Tabbed)
                            Intent intent= new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(intent);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }//termina try catch
                }
                return false;
            }
        },Utils.host+"registerRequest", jsonObject, 1, this);

    }
//0 GET
//1 POST
//2 PUT
}
