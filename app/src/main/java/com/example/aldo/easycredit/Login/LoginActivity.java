package com.example.aldo.easycredit.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aldo.easycredit.MainActivity;
import com.example.aldo.easycredit.R;
import com.example.aldo.easycredit.Utils.RequestListener;
import com.example.aldo.easycredit.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private Utils utils= new Utils();
    private EditText editTextUserName;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUserName = findViewById(R.id.editTextUserName);
        buttonLogin = findViewById(R.id.buttonLogin);

        //click de button login
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editTextUserName.getText().toString().equals("")){//se despliega toast si no ingreso username
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.empty_username),Toast.LENGTH_SHORT).show();
                }else{//inicia sesión
                    login();//iniciar sesión o registrar usuario
                }

            }//termina onclick
        });

    }//termina onCreate




    //método para iniciar sesión o registrar usuario
    public void login(){

        final JSONObject jsonObject=new JSONObject(); //para iniciar sesión se envia al servidor username, y notification_token
        try {
            jsonObject.put("username",editTextUserName.getText().toString());
            jsonObject.put("notification_token",Utils.getSharedPreferencesValue(getApplication(),Utils.utilTokenNotification));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        utils.petition(getApplicationContext(), new RequestListener() {
            @Override
            public boolean onRequestFinish(JSONObject response, final ProgressDialog progressDialog) {
                utils.requestComplete(progressDialog);
                if(response!=null){
                    //respuesta del servidor
                    try {

                        //si la respuesta del servidor es correcta
                        if(response.getString("server_response").equals("success")){

                            int userId = response.getInt("user_id"); //id del usuario
                            //se guardan username y userid sharedpreferences
                            Utils.setSharedPreferencesValue(getApplicationContext(),Utils.utilUserName,editTextUserName.getText().toString().toLowerCase());
                            Utils.setSharedPreferencesValue(getApplicationContext(),Utils.utilUserId,String.valueOf(userId));

                            //inicia actividad principal de aplicación (Tabbed)
                            Intent intent= new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(intent);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }//termina try catch
                }
                return false;
            }
        },Utils.host+"login", jsonObject, 1, this);

    }
//0 GET
//1 POST
//2 PUT


    @Override
    public void onBackPressed() {
        finishAffinity();//cerrar aplicación
    }
}
