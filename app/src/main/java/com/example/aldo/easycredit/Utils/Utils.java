package com.example.aldo.easycredit.Utils;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.StringDef;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import android.os.Handler;
import com.example.aldo.easycredit.R;


/**
 * Created by Aldo Urtusuastegui on 14/11/2018.
 */

//UTILS CONTROLA PETICIONES VOLLEY Y SHARED PREFERENCES
public class Utils implements Response.ErrorListener, Response.Listener<JSONObject>{


    public static final String utilUserId="user_id";
    public static final String utilUserName="username";


    public static final String utilToken="token";
    public static final String utilTokenNotification="token_notification";

    private RequestListener RL=null;
    private Context context=null;
    private ProgressDialog progressDialog=null;

  //  public static final String host= "http://192.168.0.14/easycredit/api/api.php?op=";
    public static final String host= "https://www.rabu.mx/easycredit/api/api.php?op="; //servidor


    @StringDef ({utilToken,utilTokenNotification,utilUserId,utilUserName})
    public @interface SharedPreferencesType{}

    public static String getSharedPreferencesValue(Context ctx, @SharedPreferencesType String identificador){ //obtener preferencias
        return getSharedPreferences(ctx).getString(identificador,"");
    }

    public static void setSharedPreferencesValue(Context ctx, @SharedPreferencesType String identificador, String valor){ //guardar preferencias
        SharedPreferences.Editor editor=getSharedPreferences(ctx).edit();
        editor.putString(identificador,valor);
        editor.apply();
    }

    public static SharedPreferences getSharedPreferences(Context ctx){
        SharedPreferences sharedPreferences=ctx.getSharedPreferences("PREFERENCES",ctx.MODE_PRIVATE);
        return  sharedPreferences;
    }

    public static SharedPreferences deleteSharedPreferences(Context ctx){
        SharedPreferences sharedPreferences=ctx.getSharedPreferences("PREFERENCES",ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor=getSharedPreferences(ctx).edit();
        editor.clear();
        editor.apply();
        return  sharedPreferences;
    }


    //Volley
    //progressDialog
    public ProgressDialog dialog(Activity activity){
        ProgressDialog progressDialog=new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.progress_dialog));
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    public void petition(final Context context, RequestListener requestListener, String api, JSONObject jsonObject, int petition, Activity activity) throws NullPointerException{
        progressDialog=null;
        if(activity!=null){
            progressDialog=dialog(activity);
        }
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        RL=requestListener;
        this.context=context;
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(petition, api, jsonObject, this, this){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return headers(context);
            }
        };
        requestQueue.add(jsonObjectRequest);
    }


    public void requestComplete(final ProgressDialog progressDialog){
        new Handler().postDelayed(new Runnable() {
            public void run() {
                progressDialog.dismiss();
            }
        }, 1000);
    }

    public HashMap<String, String> headers(Context context){
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization",getSharedPreferencesValue(context, Utils.utilToken));
        return header;
    }
    @Override
    public void onErrorResponse(VolleyError error) throws NullPointerException {
        RL.onRequestFinish(null, progressDialog);
        Log.e("Error", String.valueOf(error));
    }

    @Override
    public void onResponse(JSONObject response) throws NullPointerException {
        Log.d("Correct", String.valueOf(response));
        if(progressDialog!=null){
            RL.onRequestFinish(response, progressDialog);
        }else{
            RL.onRequestFinish(response, null);
        }

    }





}//close Utils
