package com.example.aldo.easycredit.Requests;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.aldo.easycredit.MainActivity;
import com.example.aldo.easycredit.Pojo.Request;
import com.example.aldo.easycredit.R;
import com.example.aldo.easycredit.Utils.RequestListener;
import com.example.aldo.easycredit.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//Fragmento de solicitudes pendientes
public class RequestFragment extends Fragment {

    private View view=null;
    private Utils utils= new Utils();
    private BroadcastReceiver broadcastReceiver;



    //Recyclerview
    private RecyclerView recyclerView;
    private List<Request> requestList;
    private RequestAdapter requestAdapter;

    private Button buttonNewRequest;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_request, container, false);

        //BROADCAST RECEIVER, actualiza solicitudes pendientes
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String data=intent.getStringExtra("data");
                if(data.equals("request")){
                    refreshRequest(); //refrescar solicitudes
                }

            }
        };


        //RecylerView
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        fillRequests(); //llenar solicitudes
        requestAdapter = new RequestAdapter(getContext(),requestList);
        recyclerView.setAdapter(requestAdapter);


        //Boton de nueva solicitud
        buttonNewRequest = view.findViewById(R.id.buttonNewRequest);
        buttonNewRequest.setOnClickListener(new View.OnClickListener() {// button nueva solicitud
            @Override
            public void onClick(View v) { //se manda a formulario
                Intent intent = new Intent(getContext(),NewRequestActivity.class);
                startActivity(intent);
            }
        });


        refreshRequest();

        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,new IntentFilter(MainActivity.MESSAGE));
    }

    //metodo GET para refrescar solicitudes pendientes
    public void refreshRequest(){

        utils.petition(getContext(), new RequestListener() {
            @Override
            public boolean onRequestFinish(JSONObject response, final ProgressDialog progressDialog) {

                if(response!=null){
                    try {
                        if(response.getString("server_response").equals("failure")){

                            Request.deleteAll(Request.class);
                            Request.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "REQUEST" + "'");

                        }else if(response.getString("server_response").equals("success")){ //si la respuesta es correcta

                            //eliminar request
                            Request.deleteAll(Request.class);
                            Request.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "REQUEST" + "'");
                            //Registrar de nuevo
                            JSONArray jsonArray = response.getJSONArray("request");
                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject data = jsonArray.getJSONObject(i);


                                int requestId = data.getInt("request_id");
                                Double amount = data.getDouble("amount");
                                int age = data.getInt("age");
                                int creditCard = data.getInt("credit_card");
                                int months = data.getInt("months");
                                int status = data.getInt("status");

                                //guardar  en base de datos local
                                Request newRequest = new Request(requestId,amount,age,creditCard,months,status);
                                newRequest.save();

                            }//close for

                        }//termina if success


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //refrescar recyclerview
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    fillRequests();
                    requestAdapter = new RequestAdapter(getContext(),requestList);
                    recyclerView.setAdapter(requestAdapter);


                }
                return false;
            }//termna onRequestFinish
        },Utils.host+"getRequest&user_id="+Utils.getSharedPreferencesValue(getContext(),Utils.utilUserId), null, 0, null);

    }//Termina getRequest

    //llenar requests en bdd local
    public void fillRequests(){

        //llenar requestList con información de base de datos local
        requestList = new ArrayList<>();
        List<Request> requestDb = Request.listAll(Request.class);
        for (Request requests: requestDb) {

            int requestId = requests.getRequestId();
            Double amount = requests.getAmount();
            int age = requests.getAge();
            int creditCard = requests.getCreditCard();
            int months = requests.getMonths();
            int status = requests.getStatus();

            requestList.add(new Request(requestId,amount,age,creditCard,months,status));
        }//termina foreach


        //si no tiene solicitudes
        if(requestList.isEmpty()){//Ocultar recyclerview

            LinearLayout linearEmpty = view.findViewById(R.id.requestEmpty);
            linearEmpty.setVisibility(View.VISIBLE);

            LinearLayout linearLayout = view.findViewById(R.id.request);
            linearLayout.setVisibility(View.GONE);

        }else{//mostrar recyclerview

            LinearLayout linearEmpty = view.findViewById(R.id.requestEmpty);
            linearEmpty.setVisibility(View.GONE);

            LinearLayout linearLayout = view.findViewById(R.id.request);
            linearLayout.setVisibility(View.VISIBLE);

        }

    }//fillRequests

}
