package com.example.aldo.easycredit.Profile;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.aldo.easycredit.Login.LoginActivity;
import com.example.aldo.easycredit.MainActivity;
import com.example.aldo.easycredit.R;
import com.example.aldo.easycredit.Utils.RequestListener;
import com.example.aldo.easycredit.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

//Fragmento de perfil de usuario
public class ProfileFragment extends Fragment {

    private View view=null;
    private Utils utils= new Utils();
    private BroadcastReceiver broadcastReceiver;
    private TextView textViewUsername,textViewQuantityPending;
    private Button buttonSignOff;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       // return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_profile, container, false);


        //BROADCAST RECEIVER , actualiza información de profile
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String data=intent.getStringExtra("data");
                if(data.equals("request")){
                    getQuantityPending(); //refresca cantidad de solicitudes
                }

            }
        };

        //se muestra nombre de usuario
        textViewUsername = view.findViewById(R.id.textViewUsername);
        String username = Utils.getSharedPreferencesValue(getActivity(),utils.utilUserName).toLowerCase(); //se obtiene username desde sharedpreferences
        textViewUsername.setText( getResources().getString(R.string.hello)+" "+username);


        textViewQuantityPending = view.findViewById(R.id.textViewQuantityPending); //cantidad de solicitudes pendientes

        //button cerrar sesión
        buttonSignOff = view.findViewById(R.id.buttonSignOff);
        buttonSignOff.setOnClickListener(new View.OnClickListener() {//cerrar sesión
            @Override
            public void onClick(View v) {

                //cerrar sesión, se eliminan shared preferences, excepto el token de notificación
                String tokenRefresh= Utils.getSharedPreferencesValue(getContext(),Utils.utilTokenNotification);
                Utils.deleteSharedPreferences(getContext());//eliminar preferences
                Utils.setSharedPreferencesValue(getContext(),Utils.utilTokenNotification,tokenRefresh);//guardar token refresh

                //Se redirige a login
                Intent intent = new Intent(getContext(),LoginActivity.class);
                startActivity(intent);
            }
        });

        getQuantityPending();//cargar cantidad de solicitudes pendientes

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,new IntentFilter(MainActivity.MESSAGE));
    }

    //metodo para obtener cantidad de solicitudes pendientes
    public void getQuantityPending(){

        utils.petition(getContext(), new RequestListener() {
            @Override
            public boolean onRequestFinish(JSONObject response, final ProgressDialog progressDialog) {

                if(response!=null){
                    try {
                        if(response.getString("server_response").equals("success")){ //si la respuesta es correcta

                            //cantidad de solicitudes pendientes
                            int quantityPending = response.getInt("pending");

                            if(quantityPending==0){//muestra :No tienes solicitudes pendientes
                                textViewQuantityPending.setText(getResources().getString(R.string.empty_request));
                            }else if(quantityPending==1){ //muestra : Tienes una solicitud pendiente.
                                textViewQuantityPending.setText(getResources().getString(R.string.one_request));
                            }else if(quantityPending>1){ //muestra : Tienes x solicitudes pendientes
                                textViewQuantityPending.setText(getResources().getString(R.string.have)+" "+quantityPending+" "+getResources().getString(R.string.pending_request));
                            }

                        }//termina if success


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                }
                return false;
            }//termna onRequestFinish
        },Utils.host+"countPendingRequest&user_id="+Utils.getSharedPreferencesValue(getContext(),Utils.utilUserId), null, 0, null);

    }//Termina getRequest

}
