package com.example.aldo.easycredit.Firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import com.example.aldo.easycredit.MainActivity;
import com.example.aldo.easycredit.R;


public class NotificationHandler extends ContextWrapper{

    private NotificationManager manager;

    public static final String CHANEL_HIGH_ID = "1";
    private  final  String CHANNEL_HIGH_NAME = "HIGH CHANNEL";

    public static final String CHANEL_LOW_ID = "2";
    private  final  String CHANNEL_LOW_NAME = "LOW CHANNEL";

    public NotificationHandler(Context context) {
        super(context);
        createChannels();
    }

    public NotificationManager getManager(){
        if(manager==null){
            manager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return  manager;
    }

    private void createChannels() {
        if(Build.VERSION.SDK_INT >= 26){//si le corresponde usar canales api 26 +
            //CREATING HIGH CHANNEL
            NotificationChannel highChannel = new NotificationChannel(CHANEL_HIGH_ID,CHANNEL_HIGH_NAME,NotificationManager.IMPORTANCE_HIGH);
            highChannel.enableLights(true);
            highChannel.setLightColor(Color.GREEN);
            highChannel.setShowBadge(true);

            //highChannel.enableVibration(true);
            // highChannel.setVibrationPattern(new long[]{100,200,300,400,500,400,300,200,400});
            highChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

            Uri defaulSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            highChannel.setSound(defaulSoundUri,null);



            //low
            NotificationChannel lowChannel = new NotificationChannel(CHANEL_LOW_ID,CHANNEL_LOW_NAME,NotificationManager.IMPORTANCE_LOW);

            lowChannel.enableLights(true);
            lowChannel.setLightColor(Color.YELLOW);

            lowChannel.setShowBadge(true);
            //highChannel.enableVibration(true);
            // highChannel.setVibrationPattern(new long[]{100,200,300,400,500,400,300,200,400});
            lowChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            lowChannel.setSound(defaulSoundUri,null);

            getManager().createNotificationChannel(highChannel);
            getManager().createNotificationChannel(lowChannel);

        }
    }

    public Notification.Builder createNotification(String title, String message, boolean isHighImportance){
        if(Build.VERSION.SDK_INT >=26){
            if(isHighImportance){
                return this.createNotificationWithChannel(title,message,CHANEL_HIGH_ID);
            }
            return  this.createNotificationWithChannel(title,message,CHANEL_LOW_ID);
        }
        return  this.createNotificationWithoutChannel(title,message);
    }

    private Notification.Builder createNotificationWithChannel(String title, String message, String channelId){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){

            Intent intent = new Intent(this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);

            return new Notification.Builder(getApplicationContext(),channelId)
                     .setContentTitle(title)
                     .setContentText(message)
                     .setColor(getColor(R.color.colorPrimary))
                    .setSmallIcon(R.drawable.ic_person_black_24dp)
                    .setAutoCancel(true)
                     .setContentIntent(pendingIntent);
        }

        return null;
    }


    private Notification.Builder createNotificationWithoutChannel(String title, String message){

        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);

        Uri defaulSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        return new Notification.Builder(getApplicationContext())
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_person_black_24dp)
                .setSound(defaulSoundUri)
                .setContentIntent(pendingIntent);
    }
}
