package com.example.aldo.easycredit.Utils;

import android.app.ProgressDialog;

import org.json.JSONObject;

/**
 * Created by Aldo Urtusuastegui on 14/11/2018.
 */

public interface RequestListener {
    boolean onRequestFinish(JSONObject jsonObject, ProgressDialog progressDialog);
}


