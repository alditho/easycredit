package com.example.aldo.easycredit.Login;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.aldo.easycredit.MainActivity;
import com.example.aldo.easycredit.R;
import com.example.aldo.easycredit.Utils.Utils;

public class SplashScreenActivity extends AppCompatActivity {

    private Utils utils= new Utils();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);



        //SPLASH TIME
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String username = Utils.getSharedPreferencesValue(getApplicationContext(),utils.utilUserName).toLowerCase(); //se obtiene username desde sharedpreferences

                if(username.equals("")){//si no tiene sharedpreferences se manda a que inicie sesión
                    Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(intent);
                }else{//si tiene sharedpreferences continuna con la sesión actual
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }

            }
        },2000); //tiempo de Splash

    }
}
