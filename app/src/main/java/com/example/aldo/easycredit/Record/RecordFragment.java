package com.example.aldo.easycredit.Record;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.aldo.easycredit.MainActivity;
import com.example.aldo.easycredit.Pojo.Request;
import com.example.aldo.easycredit.R;
import com.example.aldo.easycredit.Requests.RequestAdapter;
import com.example.aldo.easycredit.Utils.RequestListener;
import com.example.aldo.easycredit.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//Fragmento de historial de solicitudes
public class RecordFragment extends Fragment {

    private View view=null;
    private Utils utils= new Utils();
    private BroadcastReceiver broadcastReceiver;



    //Recyclerview
    private RecyclerView recyclerView;
    private List<Request> requestList;
    private RequestAdapter requestAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_record, container, false);


        //BROADCAST RECEIVER, actualiza información de historial
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String data=intent.getStringExtra("data");
                if(data.equals("request")){
                    refreshRecordRequest();//refrescar historial
                }

            }
        };


        //RecylerView
        recyclerView = view.findViewById(R.id.recyclerViewRecord);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        fillRecordRequests(); //llenar recyclerview
        requestAdapter = new RequestAdapter(getContext(),requestList);
        recyclerView.setAdapter(requestAdapter);



        refreshRecordRequest(); //obtener historial

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume(){
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,new IntentFilter(MainActivity.MESSAGE));
    }

    //metodo GET para obtener historial de solicitudes
    public void refreshRecordRequest(){

        utils.petition(getContext(), new RequestListener() {
            @Override
            public boolean onRequestFinish(JSONObject response, final ProgressDialog progressDialog) {

                if(response!=null){
                    try {
                        if(response.getString("server_response").equals("failure")){ //si el servidor responde failure

                            Request.deleteAll(Request.class);
                            Request.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "REQUEST" + "'");

                        }else if(response.getString("server_response").equals("success")){ //si la respuesta es correcta

                            //eliminar request
                            Request.deleteAll(Request.class);
                            Request.executeQuery("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + "REQUEST" + "'");
                            //Registrar de nuevo
                            JSONArray jsonArray = response.getJSONArray("request");
                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject data = jsonArray.getJSONObject(i);

                                //información de solicitud
                                int requestId = data.getInt("request_id");
                                Double amount = data.getDouble("amount");
                                int age = data.getInt("age");
                                int creditCard = data.getInt("credit_card");
                                int months = data.getInt("months");
                                int status = data.getInt("status");

                                //guardar  en base de datos local
                                Request newRequest = new Request(requestId,amount,age,creditCard,months,status);
                                newRequest.save();

                            }//close for

                        }//termina if success


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //refrescar recyclerview
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    fillRecordRequests();
                    requestAdapter = new RequestAdapter(getContext(),requestList);
                    recyclerView.setAdapter(requestAdapter);


                }
                return false;
            }//termna onRequestFinish
        },Utils.host+"getRecordRequest&user_id="+Utils.getSharedPreferencesValue(getContext(),Utils.utilUserId), null, 0, null);

    }//Termina getRequest

    //llenar requests en bdd local
    public void fillRecordRequests(){

        //llenar requestlist con información de base de datos local
        requestList = new ArrayList<>();
        List<Request> requestDb = Request.listAll(Request.class);
        for (Request requests: requestDb) {

            int requestId = requests.getRequestId();
            Double amount = requests.getAmount();
            int age = requests.getAge();
            int creditCard = requests.getCreditCard();
            int months = requests.getMonths();
            int status = requests.getStatus();

            requestList.add(new Request(requestId,amount,age,creditCard,months,status));
        }//termina foreach

        //si no hay historial de solicitudes
        if(requestList.isEmpty()){//Ocultar recyclerview

            LinearLayout linearEmpty = view.findViewById(R.id.recordEmpty);
            linearEmpty.setVisibility(View.VISIBLE);

            LinearLayout linearLayout = view.findViewById(R.id.record);
            linearLayout.setVisibility(View.GONE);

        }else{//mostrar recyclerview

            LinearLayout linearEmpty = view.findViewById(R.id.recordEmpty);
            linearEmpty.setVisibility(View.GONE);

            LinearLayout linearLayout = view.findViewById(R.id.record);
            linearLayout.setVisibility(View.VISIBLE);

        }


    }//fillRequests

}
