package com.example.aldo.easycredit.Firebase;

import android.app.Notification;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.aldo.easycredit.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;





public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = "NOTIFICACIÓN";

    private NotificationHandler notificationHandler;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        notificationHandler = new NotificationHandler(getApplicationContext());


        //si la notificacion tiene datos
        if(remoteMessage.getData().size() >0){

            //Si recibe una transferencia exitosa
            if(remoteMessage.getData().get("update").equals("request")){ //Actualizar solicitudes
                //valor de shared
                //se muestra notificación
                Log.d(TAG,"SE ACTUALIZO SOLICITUD");
                Notification.Builder nb = notificationHandler.createNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    notificationHandler.getManager().notify(1,nb.build());
                }
                //Actualizar información
                message(remoteMessage.getData().get("update"));
            }//termina if



        }//termina if

    }//termina onMessage


    //este metodo llama a broadcastManager que actualiza los fragmentos al recibir una actualización de información
    private void message(String data){
        Intent i = new Intent(MainActivity.MESSAGE);
        i.putExtra("data",data);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
    }//termina msj




}
