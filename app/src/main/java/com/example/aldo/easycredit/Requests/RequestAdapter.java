package com.example.aldo.easycredit.Requests;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.aldo.easycredit.Pojo.Request;
import com.example.aldo.easycredit.R;

import java.text.DecimalFormat;
import java.util.List;

//Adaptador para recyclerview de historial y de solicitudes
public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder>{

    Context context;
    List<Request> requestList;


    public RequestAdapter(Context context, List<Request> requestList){
        this.context = context;
        this.requestList = requestList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_request,parent,false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.amount.setText("$"+decimals(requestList.get(position).getAmount())); //se muestra cantidad solicitada
        holder.age.setText(String.valueOf(requestList.get(position).getAge())+" "+context.getResources().getString(R.string.years)); //edad del solicitante

        if(requestList.get(position).getCreditCard()==1){
            holder.creditCard.setText(context.getResources().getString(R.string.yes)); //si es 1 tiene tarjeta de credito
        }else{ //de lo contrario no tiene tarjeta de credito
            holder.creditCard.setText(context.getResources().getString(R.string.no));
        }

        holder.months.setText(String.valueOf(requestList.get(position).getMonths())+" "+context.getResources().getString(R.string.month)); //plazo de pago

        //en servidor se guarda la cantidad que solicito, ya que es lo mas importante, localmente se obtiene el % a pagar.
        if(requestList.get(position).getMonths()==3){//total a pagar en 3 meses
            Double totalPay=requestList.get(position).getAmount() +(requestList.get(position).getAmount()*0.05);
            holder.total.setText("$"+decimals(totalPay));
        }else if(requestList.get(position).getMonths()==6){ //total a pagar en 6 meses
            Double totalPay=requestList.get(position).getAmount() +(requestList.get(position).getAmount()*0.07);
            holder.total.setText("$"+decimals(totalPay));
        }else if(requestList.get(position).getMonths()==9){ //total a pagar en 9 meses
            Double totalPay=requestList.get(position).getAmount() +(requestList.get(position).getAmount()*0.12);
            holder.total.setText("$"+decimals(totalPay));
        }



        if(requestList.get(position).getStatus()==0){ //solicitud rechazada
            holder.status.setText(context.getResources().getString(R.string.rejected));
            holder.status.setTextColor(Color.RED);
        }else if(requestList.get(position).getStatus()==1){ //solicitud aceptada
            holder.status.setText(context.getResources().getString(R.string.accepted));
            holder.status.setTextColor(Color.rgb(0,153,51));
        }else if(requestList.get(position).getStatus()==2){ //solicitud pendiente
            holder.status.setText(context.getResources().getString(R.string.pending));
            holder.status.setTextColor(Color.rgb(204,102,0));
        }

    }//termina viewholder


    @Override
    public int getItemCount() {
        return requestList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        //se declaran elementos de recyclerview
        TextView amount,age,creditCard,months,status,total;

        public ViewHolder (final View item){
            super(item);
            amount = item.findViewById(R.id.textViewAmount);
            age = item.findViewById(R.id.textViewAge);
            creditCard = item.findViewById(R.id.textViewCreditCard);
            months = item.findViewById(R.id.textViewMonths);
            status = item.findViewById(R.id.textViewStatus);
            total = item.findViewById(R.id.textViewTotal);
        }

    }//close ViewHolder


    //regresar valor con dos decimales
    public String decimals(Double value){

        double myNumber = value;
        DecimalFormat format = new DecimalFormat("0.00");
        System.out.println(format.format(myNumber));

        return format.format(myNumber);
    }

}
