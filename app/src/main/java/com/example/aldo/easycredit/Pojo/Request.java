package com.example.aldo.easycredit.Pojo;

import com.orm.SugarRecord;

//Objeto Request que extiende sugar orm
public class Request extends SugarRecord {

    int requestId;
    Double amount;
    int age;
    int creditCard;
    int months;
    int status;

    public Request(){

    }

    public Request(int requestId,Double amount, int age, int creditCard, int months, int status){
        this.requestId = requestId;
        this.amount = amount;
        this.age = age;
        this.creditCard = creditCard;
        this.months = months;
        this.status = status;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(int creditCard) {
        this.creditCard = creditCard;
    }

    public int getMonths() {
        return months;
    }

    public void setMonths(int months) {
        this.months = months;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
